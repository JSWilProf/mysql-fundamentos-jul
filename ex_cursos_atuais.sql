-- Os Cursos onde há turmas atualmente

select c.nome as Curso, 
	   t.dt_inicio as 'Início', 
       t.dt_fim as Fim
from turmas as t,
     cursos as c
where t.dt_inicio <= now()
  and t.dt_fim >= now()
  and t.curso_id = c.id
order by c.nome desc;
  
