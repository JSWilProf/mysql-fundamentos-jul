-- Os Cursos de HTML, JAVA e C#
-- ordenados por nome em ordem decrescente
select * from cursos
where nome like '%HTML%'
   or nome like '%JAVA%'
   or nome like '%C#%'
order by nome desc;
